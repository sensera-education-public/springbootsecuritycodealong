package com.example.springbootsecuritycodealong.security;

import com.nimbusds.jose.shaded.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, jsr250Enabled = true)
@Profile("jwt")
public class JwtSecurity extends WebSecurityConfigurerAdapter {


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter
                = new
                JwtGrantedAuthoritiesConverter();
        http
                .authorizeRequests()
                .antMatchers("/api/shelves").permitAll()
                .anyRequest()
                .authenticated()
                //.hasAnyAuthority("ADMIN")
                .and()
                .oauth2ResourceServer()
                .jwt()
                .jwtAuthenticationConverter(source -> new JwtAuthenticationToken(source,
                        Stream.concat(
                                jwtGrantedAuthoritiesConverter
                                        .convert(source).stream(),
                                ((JSONArray) source.getClaimAsMap("realm_access").get("roles")).stream()
                                        .map(o -> (String) o)
                                        .map(SimpleGrantedAuthority::new)
                        ).collect(Collectors.toList())));
    }
}
