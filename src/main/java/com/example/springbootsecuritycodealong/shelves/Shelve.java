package com.example.springbootsecuritycodealong.shelves;

import lombok.Value;

@Value
public class Shelve {
    String name;
}
