package com.example.springbootsecuritycodealong.shelves;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/shelves")
public class ShelvesController {

    @GetMapping
    public List<Shelve> all() {
        return List.of(
                new Shelve("AAA")
        );
    }
}
