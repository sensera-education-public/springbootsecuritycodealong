package com.example.springbootsecuritycodealong;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSecurityCodealongApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSecurityCodealongApplication.class, args);
    }

}
