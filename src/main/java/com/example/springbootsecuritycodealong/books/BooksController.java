package com.example.springbootsecuritycodealong.books;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/api/books")
public class BooksController {

    //@RolesAllowed("ADMIN")
    @PreAuthorize("hasAnyAuthority('USER') or hasAnyRole('USER')")
    @GetMapping
    public List<Book> all() {
        return List.of(
                new Book("ABC")
        );
    }
}
