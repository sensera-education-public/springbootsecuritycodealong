package com.example.springbootsecuritycodealong.books;

import lombok.Value;

@Value
public class Book {
    String name;
}
